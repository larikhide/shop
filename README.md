# how to

1. создать инструкцию .gitlabci.yaml запушить в репозиторий
2. в настройках CI/CD отключить запуск на общественных раннерах, если предполагается запускать на своем
3. прочесть инструкцию как создать свой раннер на той же странице
4. `gitlab runner register `
http://gitlab.com/
токен из настроек CI/CD
ввести имя раннера
теги можно скипнуть (можно настроить какие стейджи будет брать раннер)
указать базовый образ на котором будет крутиться раннер - выбираем docker
указать имедж golang:latest
5. открыть вкладку раннерс в настройкаx CI/CD и увидеть там свой раннер. 
`gitlab-runner start` - запустить раннер
`gitlab-runner status`
6. идем в пайплайн на гитлабе и перезапускаем стейдж, теперь он запустится не на общественном раннере , а на нашем